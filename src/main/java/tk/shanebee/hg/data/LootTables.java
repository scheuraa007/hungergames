package tk.shanebee.hg.data;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;
import tk.shanebee.hg.HG;
import tk.shanebee.hg.util.ChestItem;
import tk.shanebee.hg.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LootTables {

    private HG plugin;
    private FileConfiguration basic_chest = null;
    private File basic_chestFile = null;

    private FileConfiguration bonus_chests = null;
    private File bonus_chestsFile = null;

    //ChestBlockTypes

    public static Material normal_chest;
    public static List<Material> bonus_chestslist;
    public static int amountbonuschests;

    //Items List
    public static List<ChestItem> normal_chestItems;
    public static List<List<ChestItem>> bonus_chest_items;

    public static int maxnormal, minnormal;

    public static List<Integer> maxbonus,minbonus;


    public LootTables(HG plugin) {
        this.plugin = plugin;
        bonus_chestslist = new ArrayList<>();
        maxbonus = new ArrayList<>();
        minbonus = new ArrayList<>();
        normal_chestItems = new ArrayList<>();
        bonus_chest_items = new ArrayList<>();
        if (basic_chestFile == null) {
            basic_chestFile = new File(plugin.getDataFolder(), "basic_chest.yml");
        }
        if (!basic_chestFile.exists()) {
            plugin.saveResource("basic_chest.yml", false);
            basic_chest = YamlConfiguration.loadConfiguration(basic_chestFile);
            Util.log("&7New basic_chest.yml created");
        } else {
            basic_chest = YamlConfiguration.loadConfiguration(basic_chestFile);
        }

        if (bonus_chestsFile == null) {
            bonus_chestsFile = new File(plugin.getDataFolder(), "bonus_chests.yml");
        }
        if (!bonus_chestsFile.exists()) {
            plugin.saveResource("bonus_chests.yml", false);
            bonus_chests = YamlConfiguration.loadConfiguration(bonus_chestsFile);
            Util.log("&7New bonus_chests.yml created");
        } else {
            bonus_chests = YamlConfiguration.loadConfiguration(bonus_chestsFile);
        }
        //Get Material Types of Normal chest from Config. TO-DO: List more ChestMaterials on One Chest
        YamlConfiguration normal_chestcfg = YamlConfiguration.loadConfiguration(basic_chestFile);
        normal_chest = Material.valueOf(normal_chestcfg.getString("normal_chest.type"));

        //Get Items for normal Chest
        ArrayList<String> items = (ArrayList<String>) normal_chestcfg.getStringList("normal_chest.items");
        for (String item : items) {

            String[] split2 = item.split("%");
            String[] split = split2[0].split("-");

            ItemStack itemStack = null;
            Material mat = Material.valueOf(split[0]);
            itemStack = new ItemStack(mat);
            if (split2.length > 1) {
                String[] hoehe = split2[1].split(":");
                String enchantment = hoehe[0];
                Enchantment enchantmentname = Enchantment.getByName(enchantment);
                int enchid;
                if (hoehe.length > 1) {
                    enchid = Integer.parseInt(hoehe[1]);
                } else {
                    enchid = 1;
                }
                if (enchantmentname!= null){
                    itemStack.addUnsafeEnchantment(enchantmentname, enchid);
            }
            }
            int min = Integer.parseInt(split[1]);
            int max = Integer.parseInt(split[2]);
            int prob = Integer.parseInt(split[3]);
            normal_chestItems.add(new ChestItem(itemStack, min, max, prob));
        }

        //Get Min and Max for normal Chest
        minnormal = normal_chestcfg.getInt("normal_chest.min-items");
        maxnormal = normal_chestcfg.getInt("normal_chest.max-items");

        //Get Material Types of Bonus chest from Config. TO-DO: List more ChestMaterials on One Chest
        YamlConfiguration bonus_chestcfg = YamlConfiguration.loadConfiguration(bonus_chestsFile);
        amountbonuschests = bonus_chestcfg.getInt("amountchest");
        for (int i = 1; i <= amountbonuschests; i++) {
            bonus_chestslist.add(Material.valueOf(bonus_chestcfg.getString("bonus_" + i + ".type")));
        }

        for (int i = 1; i <= amountbonuschests; i++) {
            ArrayList<String> itemsbonus = (ArrayList<String>) bonus_chestcfg.getStringList("bonus_" + i + ".items");
             ArrayList<ChestItem> bonuslist= new ArrayList<>();
            for (String item : itemsbonus) {
                String[] split2 = item.split("%");
                String[] split = split2[0].split("-");

                ItemStack itemStack = null;
                Material mat = Material.valueOf(split[0]);
                itemStack = new ItemStack(mat);
                if (split2.length > 1) {
                    String[] hoehe = split2[1].split(":");
                    String enchantment = hoehe[0];
                    Enchantment enchantmentname = Enchantment.getByName(enchantment);
                    int enchid;
                    if (hoehe.length > 1) {
                        enchid = Integer.parseInt(hoehe[1]);
                    } else {
                        enchid = 1;
                    }
                    if (enchantmentname!= null){
                        itemStack.addUnsafeEnchantment(enchantmentname, enchid);
                    }
                }
                int min = Integer.parseInt(split[1]);
                int max = Integer.parseInt(split[2]);
                int prob = Integer.parseInt(split[3]);
                bonuslist.add(new ChestItem(itemStack,min,max,prob));
                for(ChestItem normal : normal_chestItems){
                    bonuslist.add(normal);
                }

            }

            bonus_chest_items.add(bonuslist);


        }

        //Get Max and Min for bonus Chests
        for(int i=1;i<=amountbonuschests;i++){
            minbonus.add(bonus_chestcfg.getInt("bonus_"+i+".min-items"));
            maxbonus.add(bonus_chestcfg.getInt("bonus_"+i+".max-items"));
        }

    }
}
